#!/usr/bin/perl
use Digest::HMAC_SHA1 qw(hmac_sha1 hmac_sha1_hex);
use JSON;
use strict;
use warnings;

my $appId = "fr.freebox.desktop";
my $appName = "Desktop Streamer";
my $appVersion = "0.0.1";
my $appHostname = "Desktop";
my $curl_get = "curl -s ";
my $curl_post = "curl -s -X POST -H 'application/json' -d ";

my $freebox_ip = "mafreebox.freebox.fr";
my $url_api_version = " http://$freebox_ip/api_version";
my $url_login_authorize = " http://$freebox_ip/api/v3/login/authorize";
my $url_login_session = " http://$freebox_ip/api/v3/login/session";
my $url_player = " http://mafreebox.freebox.fr/api/v3/airmedia/receivers/Freebox%20Player/";

my $json_cmd_version = from_json qx{$curl_get$url_api_version};

my $json_auth = {'app_id'        => $appId,
                'app_name'      => $appName,
                'app_version'   => $appVersion,
                'device_name'   => $appHostname};
            
my $data_auth = encode_json $json_auth;

my $auth_cmd = $curl_post."'".$data_auth."'".$url_login_authorize;
my $res_auth = from_json `$auth_cmd`;

my $track_id = ${$res_auth}{'result'}{'track_id'};
my $app_token = ${$res_auth}{'result'}{'app_token'};
my $url_auth2 = $curl_get.$url_login_authorize."/".$track_id;
my $res_auth2 = from_json `$url_auth2`;
my $status = ${$res_auth2}{'result'}{'status'};
while ($status ne "granted")
{
    print "waiting auth ...\n";
    $res_auth2 = from_json `$url_auth2`;
    $status = ${$res_auth2}{'result'}{'status'};
}

my $password_salt = ${$res_auth2}{'result'}{'password_salt'};
my $challenge = ${$res_auth2}{'result'}{'challenge'};

my $hash = hmac_sha1_hex($challenge, $app_token);

my $json_auth2 = {  'app_id'        => $appId,
                    'password'      => $hash};
                    
my $data_auth2 = encode_json $json_auth2;
my $auth_cmd2 = $curl_post."'".$data_auth2."'".$url_login_session;

my $res_auth_perm = `$auth_cmd2`;
my $perms = from_json $res_auth_perm;
my $session_token = ${$perms}{'result'}{'session_token'};
my $extra_header = "X-Fbx-App-Auth: $session_token";
my $curl_post_auth = "curl -s -X POST -H 'Content-Type: application/json' -H '$extra_header' -d ";
#'#transcode{vcodec=mp4v,acodec=mpga,vb=800,ab=128}:standard{access=http,mux=ogg,dst=server.example.org:8080}' 
#'#transcode{vcodec=mpeg4,vb=4000000,acodec=mpga,ab=256,scale=1,width=1920,height=1080}:rtp{mux=ts,ttl=10,name='Desktop',port=1234,sdp=rtsp://192.168.0.25:8080/test.sdp}'
my $vlc_cmd = "cvlc screen:// :screen-fps=10 :screen-caching=100 --sout '#transcode{vcodec=mp4v,acodec=mpga,vb=800,ab=128}:standard{access=http,mux=ogg,dst=192.68.0.25:8080}'  &";
#system($vlc_cmd);

my $json_player = { 'action'=>'start',
                    'media_type'=>'video',
                    'media'=>'http://oktail.org/gallery/divers/anime%20video/Ranma%20-%20500%20Miles.mpg'};
my $data_player = encode_json $json_player;
my $player_cmd = $curl_post_auth."'".$data_player."'".$url_player;
my $res_player = `$player_cmd`;
