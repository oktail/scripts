#!/usr/bin/perl -w
#use strict;
#use URI::Escape;

sub bytitle
{
    chomp $a;
    my @decoup = split '/',$a;
    my $manga_a = pop @decoup;
    chomp $b;
    my @decoup2 = split '/',$b;
    my $manga_b = pop @decoup2;
    $manga_b cmp $manga_a;
}

my @root_first;

if ($ARGV[0] eq "get")
{
    @root_first = `curl -s http://www.mymanga.io/mangas/ | grep href=\\\"mangas | cut -d '"' -f 2 | cut -d / -f 2 | sort | uniq | grep -i $ARGV[1]`;
}
elsif ($ARGV[0] eq "search")
{
    system("curl -s http://www.mymanga.io/mangas/ | grep href=\\\"mangas | cut -d '\"' -f 2 | cut -d / -f 2 | sort | uniq | grep -i $ARGV[1]");
}
else
{
    @root_first = `curl -s http://www.mymanga.io/mangas/ | grep href=\\\"mangas | cut -d '"' -f 2 | cut -d / -f 2 | sort | uniq`;
}
my @root = sort bytitle @root_first;
my $manga;
my $chapter;
my $page;
my $total = scalar @root;
my $current = 0;
foreach $manga (@root)
{
    $current++;
    chomp $manga;
    my @decoup = split '/',$manga;
    my $manga_name = pop @decoup;
    if ($manga_name ne "")
    {
	my @decoup2 = split '\.',$manga_name;
	$manga_name = $decoup2[0];
	chomp $manga_name;
	if (! -d $manga_name)
	{
	    system("mkdir $manga_name");
	}
	print "(" . (int(10000*$current/$total)/100) . ") " . $manga_name . "\n";
	my @manga_page = `curl -s http://www.topmanga.eu/$manga_name/ | grep Chapitre | grep option | cut -d '"' -f 2`;
	my $i = 0;
	foreach $chapter (reverse @manga_page)
	{
	    $i++;
	    chomp $chapter;
	    my $num = `echo $chapter | cut -d '/' -f 3`;
	    print $num;
	    chomp $num;
	    my $paddednum = sprintf( "%05d", $i);
	    if (! -e $manga_name . "/" . $manga_name . "_vol_" . $paddednum . ".cbr")
	    {
		system("mkdir $manga_name/$num \n");
		my @page_list = `curl -s http://www.topmanga.eu/$manga_name/$num/ | grep option | grep data-page | cut -d '\"' -f 2`;
		foreach $page (@page_list)
		{
		    chomp $page;
#		    print "curl -s http://www.topmanga.eu/$page | grep img | grep $manga_name | cut -d '\"' -f 6\n";
		    my $img = `curl -s http://www.topmanga.eu/$page | grep img | grep $manga_name | cut -d '"' -f 6`;
		    chomp $img;
		    my $ext = `echo $img | rev | cut -d . -f 1 | rev`;
		    chomp $ext;
		    my $id = `echo $page | rev | cut -d / -f 1 | rev`;
		    chomp $id;
		    my $paddedid = sprintf( "%05d", $id);
		    print $paddedid . "...";
		    system("wget -O $manga_name/$num/$paddedid.$ext -q $img");
		    print " OK\n";
		}

		system("cd " . $manga_name . "/" . $num . "/ ; rar a ../" . $manga_name . "_vol_" . $paddednum . ".cbr * ; cd -");
		system("rm -rf " . $manga_name . "/" . $num);
	    }
	}
    }
}
