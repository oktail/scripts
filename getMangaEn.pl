#!/usr/bin/perl -w
#use strict;
chdir('/var/www/oktail.org/download/private/manga');

sub bytitle
{
    chomp $a;
    my @decoup = split '/',$a;
    my $manga_a = pop @decoup;
    chomp $b;
    my @decoup2 = split '/',$b;
    my $manga_b = pop @decoup2;
    $manga_a cmp $manga_b;
}

my @root_first;

if ($ARGV[0] eq "get")
{
    @root_first = `curl -s www.mangareader.net/alphabetical | grep li | grep href | cut -d '"' -f 2 | grep -v http | sort -n | grep / | sed 's/^/http:\\/\\/www.mangareader.net/g' | grep -i $ARGV[1]`;
}
elsif ($ARGV[0] eq "search")
{
    system("curl -s www.mangareader.net/alphabetical | grep li | grep href | cut -d '\"' -f 2 | grep -v http | sort -n | grep / | grep -i $ARGV[1]");
}
else
{
    @root_first = `curl -s www.mangareader.net/alphabetical | grep li | grep href | cut -d '"' -f 2 | grep -v http | sort -n | grep / | sed 's/^/http:\\/\\/www.mangareader.net/g'`;
}
my @root = sort bytitle @root_first;
my $manga;
my $chapter;
my $page;
my $total = scalar @root;
my $current = 0;
foreach $manga (@root)
{
    $current++;
    chomp $manga;
    my @decoup = split '/',$manga;
    my $manga_name = pop @decoup;
    if ($manga_name ne "www.mangareader.net")
    {
	my @decoup2 = split '\.',$manga_name;
	$manga_name = $decoup2[0];
	
	if (! -d $manga_name)
	{
	    system("mkdir $manga_name");
	}
	print "(" . (int(10000*$current/$total)/100) . ") " . $manga_name . "\n";
	my @manga_page = `curl -s $manga | grep $manga_name | grep href | cut -d '"' -f 2 | grep ^/ | sort -n | uniq | sed 's/^/http:\\/\\/www.mangareader.net/g'`;
#	my @manga_page = `curl -s $manga | grep $decoup[3] | grep href | cut -d '"' -f 2 | grep ^/ | sort -n | uniq | sed 's/^/http:\\/\\/www.mangareader.net/g'`;
	my $i = 0;
	foreach $chapter (@manga_page)
	{
	    $i++;
	    chomp $chapter;
	    my @decoup3 = split '/',$chapter;
	    my $head = pop @decoup3;
	    my @decoup4 = split '\.',$head;
	    my $num = $decoup4[0];
	    my $paddednum = sprintf( "%05d", $i);
	    if (! -e $manga_name . "/" . $manga_name . "_vol_" . $paddednum . ".cbr")
	    {
		system("mkdir $manga_name/$num");
		my @page_list = `curl -s $chapter  | sed 's/></>\\n</g' | grep option | cut -d '"' -f 2 | sed 's/^/http:\\/\\/www.mangareader.net/g'`;
		foreach $page (@page_list)
		{
		    chomp $page;
		    my $image = `curl -s $page | grep document | grep pu | cut -d "'" -f 4 | grep jpg`; #hasardeux
		    chomp $image;
		    my @dec = split '/',$image;
		    my $dest = pop @dec;
		    if (defined $dest)
		    {
			if (! -e $manga_name . "/" . $num . "/" . $dest)
			{
			    print $manga_name . "/" . $num . "/" . $dest . " ...";
			    system("wget -q '" . $image . "' -O " . $manga_name . "/" . $num . "/" . $dest);
			    print " OK\n";
			}
		    }
		}
		system("cd " . $manga_name . "/" . $num . "/ ; rar a ../" . $manga_name . "_vol_" . $paddednum . ".cbr * ; cd -");
		system("rm -rf " . $manga_name . "/" . $num);
	    }
	}
    }
}
